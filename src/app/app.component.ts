import {Component} from '@angular/core';
import {TextDecoder, TextEncoder} from 'text-encoding';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent {
    constructor() {
        if (!window['TextDecoder']) {
            window['TextDecoder'] = TextDecoder;
        }
        if (!window['TextEncoder']) {
            window['TextEncoder'] = TextEncoder;
        }
    }
}

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RegisterComponent} from "./register/register.component";
import {TeamsComponent} from "./code-names/game/teams/teams.component";
import {BoardComponent} from "./code-names/game/board/board.component";
import {GameGuard} from "./code-names/game/game.guard";
import {UnoGameGuard} from "./uno/game/uno-game.guard";
import {UnoPlayersComponent} from "./uno/game/players/uno-players.component";
import {UnoBoardComponent} from "./uno/game/board/uno-board.component";
import {GamesComponent} from "./games/games.component";
import {UserGuard} from './user.guard';

const routes: Routes = [
    {
        path: '',
        component: RegisterComponent
    },
    {
        path: 'games',
        component: GamesComponent,
        canActivate: [UserGuard]
    },
    {
        path: 'code-names',
        children: [
            {
                path: 'game/:guuid/players',
                canActivate: [GameGuard],
                component: TeamsComponent
            },
            {
                path: 'game/:guuid/board',
                canActivate: [GameGuard],
                component: BoardComponent
            }
        ]
    },
    {
        path: 'uno',
        children: [
            {
                path: 'game/:guuid/players',
                canActivate: [UnoGameGuard],
                component: UnoPlayersComponent
            },
            {
                path: 'game/:guuid/board',
                canActivate: [UnoGameGuard],
                component: UnoBoardComponent
            }
        ]
    },
    // otherwise redirect to home
    {
        path: '**',
        redirectTo: ''
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}

import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Injectable} from "@angular/core";
import {UserInfoService} from './services/user-info.service';

@Injectable()
export class UserGuard implements CanActivate {
    constructor(private userInfoService: UserInfoService,
                private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree {
        if (this.userInfoService.me.name == null || this.userInfoService.me.icon == null) {
            return this.router.createUrlTree(["/"]);
        } else {
            return true;
        }
    }
}

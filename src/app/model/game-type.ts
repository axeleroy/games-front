export enum GameType {
    UNO = 'UNO',
    CODE_NAMES = 'CODE_NAMES'
}

export const GameTypePath = new Map<GameType, string>([
    [GameType.UNO, 'uno'],
    [GameType.CODE_NAMES, 'code-names'],
]);

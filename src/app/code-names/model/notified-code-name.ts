import {CodeNameColor} from "./code-name-color";

export interface NotifiedCodeName {
    name: string;
    color: CodeNameColor;
}

import {CodeNameColor} from "./code-name-color";
import {User} from "../../model/user";

export interface Spy extends User {
    team?: CodeNameColor;
    spyMaster?: boolean;
}

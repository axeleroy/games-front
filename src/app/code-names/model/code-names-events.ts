export enum CodeNamesEvents {
    add_player = 'add_player',
    launch_game = 'launch_game',
    next_turn = 'next_turn',
    submit_hint = 'submit_hint',
    submit_guess = 'submit_guess',
    new_game = 'new_game'
}

import {CodeNameColor} from "./code-name-color";

export interface NotifiedEnd {
    winningTeam: CodeNameColor;
    blackFound: boolean;
    otherFound: boolean;
}

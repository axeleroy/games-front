import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";
import {Spy} from "../model/spy";
import {RequestHint} from "../model/request-hint";
import {NotifiedGame} from "../model/notified-game";
import {DEFAULT_HEADER} from "../../model/http-header";
import {GameType, GameTypePath} from "../../model/game-type";
import {CodeNamesEvents} from "../model/code-names-events";

@Injectable({providedIn: 'root'})
export class ActionsService {
    private readonly baseUrl: string;


    constructor(private httpClient: HttpClient) {
        this.baseUrl = environment.apiUrl + '/' + GameTypePath.get(GameType.CODE_NAMES)
    }

    updateUser(guuid: string, spy: Spy): Observable<void> {
        return this.httpClient.post<void>(`${this.baseUrl}/game/${guuid}/user/${spy.uuid}/${CodeNamesEvents.add_player}`, spy, DEFAULT_HEADER);
    }

    replayUser(guuid: string, uuid: string): Observable<NotifiedGame> {
        return this.httpClient.get<NotifiedGame>(`${this.baseUrl}/game/${guuid}/user/${uuid}/replay`, DEFAULT_HEADER);
    }

    startNewGame(guuid: string, uuid: string): Observable<void> {
        return this.httpClient.post<void>(`${this.baseUrl}/game/${guuid}/user/${uuid}/${CodeNamesEvents.new_game}`, null, DEFAULT_HEADER);
    }

    launchGame(guuid: string, uuid: string): Observable<void> {
        return this.httpClient.post<void>(`${this.baseUrl}/game/${guuid}/user/${uuid}/${CodeNamesEvents.launch_game}`, null, DEFAULT_HEADER);
    }

    submitHint(guuid: string, uuid: string, hint: RequestHint): Observable<void> {
        return this.httpClient.post<void>(`${this.baseUrl}/game/${guuid}/user/${uuid}/${CodeNamesEvents.submit_hint}`, hint, DEFAULT_HEADER);
    }

    submitGuess(guuid: string, uuid: string, position: number): Observable<void> {
        return this.httpClient.post<void>(`${this.baseUrl}/game/${guuid}/user/${uuid}/${CodeNamesEvents.submit_guess}`, {element: position}, DEFAULT_HEADER);
    }

    nextTurn(guuid: string, uuid: string): Observable<void> {
        return this.httpClient.post<void>(`${this.baseUrl}/game/${guuid}/user/${uuid}/${CodeNamesEvents.next_turn}`, null, DEFAULT_HEADER);
    }
}

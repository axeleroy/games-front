import {Component, Input} from "@angular/core";
import {CodeNameColor} from "../../../model/code-name-color";
import {Spy} from "../../../model/spy";

@Component({
    selector: 'app-team',
    templateUrl: './team.component.html'
})
export class TeamComponent {
    @Input("team") team: CodeNameColor;
    @Input("spies") spies: Spy[];

    CodeNameColor = CodeNameColor;
}

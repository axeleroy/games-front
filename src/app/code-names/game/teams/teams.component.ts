import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserInfoService} from "../../../services/user-info.service";
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {CodeNameColor} from "../../model/code-name-color";
import {Spy} from "../../model/spy";
import {ActionsService} from "../../services/actions.service";
import {GameService} from "../../services/game.service";
import {GameGuard} from "../game.guard";

@Component({
    selector: 'app-game',
    templateUrl: './teams.component.html'
})
export class TeamsComponent implements OnInit, OnDestroy {
    me: Spy;
    spies: Spy[];
    guuid: string;
    name: string;
    canStart = false;
    CodeNameColor = CodeNameColor;

    private gameSub: Subscription;

    constructor(private gameService: GameService,
                private userInfoService: UserInfoService,
                private actionsService: ActionsService,
                private activatedRoute: ActivatedRoute,
                private router: Router,
                private gameGuard: GameGuard) {
    }

    ngOnInit(): void {
        this.me = {...this.userInfoService.me};
        this.guuid = this.activatedRoute.snapshot.params.guuid;
        this.gameSub = this.gameService.game$.subscribe((game) => {
            this.name = game.name;
            this.spies = game.spies;
            this.me = game.spies.find((s) => s.uuid === this.me.uuid);
            this.canStart = game.canStartGame;
            this.gameGuard.redirect(game, this.guuid);
        });
    }

    ngOnDestroy(): void {
        this.gameSub.unsubscribe();
    }

    onStartGame(): void {
        this.actionsService.launchGame(this.guuid, this.me.uuid).subscribe();
    }

    onJoinTeam(color: CodeNameColor) {
        this.me.team = color;
        this.actionsService.updateUser(this.guuid, this.me).subscribe();
    }

    onMasterPush() {
        this.me.spyMaster = true;
        this.actionsService.updateUser(this.guuid, this.me).subscribe();
    }
}

import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {Component} from "@angular/core";

@Component({
    selector: 'app-validation-modal',
    templateUrl: './validation-modal.component.html'
})
export class ValidationModalComponent {

    constructor(public modal: NgbActiveModal) {
    }

    onClicked(out: boolean) {
        this.modal.close(out);
    }
}

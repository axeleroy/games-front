import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {GameInfo} from "../model/game-info";
import {HttpClient} from "@angular/common/http";
import {GameType, GameTypePath} from "../model/game-type";
import {DEFAULT_HEADER} from "../model/http-header";
import {environment} from "../../environments/environment";
import {Subscription, timer} from "rxjs";
import {switchMap} from "rxjs/operators";
import {UserInfoService} from "../services/user-info.service";

@Component({
    selector: 'app-games',
    templateUrl: './games.component.html'
})
export class GamesComponent implements OnInit, OnDestroy {
    games: GameInfo[];
    guuid: string;
    filter: string = null;
    GameType = GameType;

    private sub: Subscription;

    constructor(private activatedRoute: ActivatedRoute,
                private userInfoService: UserInfoService,
                private httpClient: HttpClient) {
    }

    ngOnInit(): void {
        this.games = this.activatedRoute.snapshot.data.games;
        this.sub = timer(0, 5000)
            .pipe(
                switchMap(() => {
                    return this.httpClient.get<GameInfo[]>(`${environment.apiUrl}/games`,
                        {
                            ...DEFAULT_HEADER,
                            params: {uuid: this.userInfoService.uuid}
                        });
                })
            )
            .subscribe((games) => {
                this.games = games;
            });
    }

    ngOnDestroy(): void {
        if (this.sub) {
            this.sub.unsubscribe();
        }
    }

    onNewGame(type: GameType): void {
        this.httpClient.post<any>(`${environment.apiUrl}/${GameTypePath.get(type)}/game/`, null, DEFAULT_HEADER)
            .subscribe((json) => {
                this.guuid = json.guuid;
                this.games.push({guuid: json.guuid, name: json.name, joinable: true, playerNames: null, type: type});
            });
    }
}

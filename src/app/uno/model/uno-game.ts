import {UnoPlayer} from "./uno-player";
import {UnoCard} from "./uno-card";
import {UnoColor} from "./uno-color";

export interface UnoGame {
    name: string;

    players: UnoPlayer[];
    cards: UnoCard[];

    gameEnded: boolean;
    gameStarted: boolean;

    canUno: boolean;
    canCounter: boolean;
    sum: number;

    cardPlayed: UnoCard;
    selectedColor?: UnoColor;
}

export enum UnoEvents {
    add_player = 'add_player',
    launch_game = 'launch_game',
    uno = 'uno',
    counter = 'counter',
    play_card = 'play_card',
    reset_score = 'reset_score',
    cannot_play = 'cannot_play'
}

export enum UnoColor {
    YELLOW = 'YELLOW',
    GREEN = 'GREEN',
    RED = 'RED',
    BLUE = 'BLUE',
    ALL = 'ALL'
}

export const ALL_COLORS = [
    UnoColor.YELLOW,
    UnoColor.GREEN,
    UnoColor.RED,
    UnoColor.BLUE
];

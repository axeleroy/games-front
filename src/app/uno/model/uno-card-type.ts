export enum UnoCardType {
    NUMBER = 'NUMBER',
    REVERSE = 'REVERSE',
    BLOCK = 'BLOCK',
    PLUS_TWO = 'PLUS_TWO',
    PLUS_FOUR = 'PLUS_FOUR',
    JOKER = 'JOKER'
}

export const ALL_TYPES = [
    UnoCardType.NUMBER,
    UnoCardType.REVERSE,
    UnoCardType.BLOCK,
    UnoCardType.PLUS_TWO,
    UnoCardType.PLUS_FOUR,
    UnoCardType.JOKER
];

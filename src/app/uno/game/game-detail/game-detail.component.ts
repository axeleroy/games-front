import {Component, Input} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {UserInfoService} from "../../../services/user-info.service";
import {GameInfo} from "../../../model/game-info";
import {UnoActionsService} from "../../services/uno-actions.service";
import {UnoGameService} from "../../services/uno-game.service";
import {UnoGameGuard} from "../uno-game.guard";

@Component({
    selector: 'app-uno-game',
    templateUrl: './game-detail.component.html'
})
export class UnoGameDetailComponent {
    @Input()
    game: GameInfo;

    constructor(private activatedRoute: ActivatedRoute,
                private actionsService: UnoActionsService,
                private userInfoService: UserInfoService,
                private gameService: UnoGameService,
                private router: Router,
                private gameGuard: UnoGameGuard) {
    }

    onJoin(game: GameInfo) {
        this.gameService.gameId = game.guuid;
        this.actionsService.updateUser(game.guuid, this.userInfoService.me)
            .subscribe(() => {
                this.router.navigate(['/uno', 'game', game.guuid, 'players']);
            });
    }

    onReplay(game: GameInfo) {
        this.actionsService.replayUser(game.guuid, this.userInfoService.uuid)
            .subscribe((unoGame) => {
                this.gameGuard.redirect(unoGame, game.guuid);
            })
    }
}

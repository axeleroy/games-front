import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Injectable} from "@angular/core";
import {Observable, of} from "rxjs";
import {UserInfoService} from "../../services/user-info.service";
import {map} from "rxjs/operators";
import {UnoActionsService} from "../services/uno-actions.service";
import {UnoGameService} from "../services/uno-game.service";
import {UnoGame} from "../model/uno-game";

@Injectable()
export class UnoGameGuard implements CanActivate {
    constructor(private gameService: UnoGameService,
                private actionsService: UnoActionsService,
                private userInfoService: UserInfoService,
                private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> {
        if (this.gameService.gameId != null) {
            return of(true);
        } else {
            const guuid = route.params.guuid;
            return this.actionsService.replayUser(guuid, this.userInfoService.uuid)
                .pipe(
                    map((notifiedGame) => {
                        if (notifiedGame != null) {
                            this.gameService.gameId = guuid;
                            this.gameService.publishGame(notifiedGame);
                        }
                        return this.router.createUrlTree(this.buildRedirect(notifiedGame, guuid));
                    })
                );
        }
    }

    redirect(unoGame: UnoGame, guuid): void {
        const route = this.buildRedirect(unoGame, guuid);
        const tree = this.router.createUrlTree(route);
        if (!this.router.isActive(tree, true)) {
            this.router.navigate(route);
        }
    }

    private buildRedirect(unoGame: UnoGame, guuid: string): any[] {
        if (unoGame != null) {
            if (!unoGame.gameStarted) {
                return ['/uno', 'game', guuid, 'players'];
            } else {
                return ['/uno', 'game', guuid, 'board'];
            }
        } else {
            return ['/uno', 'games'];
        }
    }

}

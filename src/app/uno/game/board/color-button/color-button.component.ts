import {Component, ElementRef, EventEmitter, Input, OnInit, Output} from "@angular/core";

@Component({
    selector: 'app-uno-color-button',
    templateUrl: './color-button.component.html',
    styleUrls: ['./color-button.component.sass']
})
export class UnoColorButtonComponent implements OnInit {

    @Input() colorSetting: any;
    @Output() clicked = new EventEmitter<void>();

    constructor(private current: ElementRef) {
    }

    ngOnInit(): void {
        const currentStyle = this.current.nativeElement.style;
        currentStyle.setProperty('--uno-green', this.colorSetting.GREEN);
        currentStyle.setProperty('--uno-yellow', this.colorSetting.YELLOW);
        currentStyle.setProperty('--uno-blue', this.colorSetting.BLUE);
        currentStyle.setProperty('--uno-red', this.colorSetting.RED);
    }
}

import {Component, EventEmitter, Input, Output} from "@angular/core";
import {UnoCardType} from "../../../model/uno-card-type";
import {UnoCard} from "../../../model/uno-card";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {UnoColor} from "../../../model/uno-color";
import {ColorSelectionModalComponent} from "./color-selection-modal/color-selection-modal.component";

@Component({
    selector: 'app-uno-card',
    templateUrl: './uno-card.component.html',
    styleUrls: ['./uno-card.component.sass']
})
export class UnoCardComponent {
    UnoCardType = UnoCardType;

    @Input() card: UnoCard;
    @Input() color: UnoColor;
    @Input() active: boolean;
    @Output() clicked = new EventEmitter<{ cardId: string; color?: UnoColor }>();

    constructor(private _modalService: NgbModal) {
    }

    onCardClicked() {
        if (this.active) {
            if (this.card.color === UnoColor.ALL) {
                this._modalService.open(ColorSelectionModalComponent)
                    .result.then((value: UnoColor) => this.clicked.emit({cardId: this.card.uuid, color: value}));
            } else {
                this.clicked.emit({cardId: this.card.uuid})
            }
        }
    }
}

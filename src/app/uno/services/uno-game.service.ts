import {Injectable} from "@angular/core";
import {Message} from "@stomp/stompjs";
import {BehaviorSubject, Observable} from "rxjs";
import {WebsocketProviderService} from "../../services/websocket-provider.service";
import {filter} from "rxjs/operators";
import {UnoGame} from "../model/uno-game";

@Injectable({providedIn: 'root'})
export class UnoGameService {
    private readonly game;

    readonly game$: Observable<UnoGame>;
    private guuid: string;

    constructor(private webSocketService: WebsocketProviderService) {
        this.game = new BehaviorSubject<UnoGame>(null);
        this.game$ = this.game.asObservable().pipe(filter((value) => value != null));
    }

    publishGame(game: UnoGame): void {
        this.game.next(game);
    }

    set gameId(guuid: string) {
        this.guuid = guuid;
        this.webSocketService.clearSubscriptions();
        this.webSocketService.addSubscription(`/uno/game/${guuid}`, (payload: Message) => {
            this.game.next(JSON.parse(payload.body));
        }, true);
    }

    get gameId(): string {
        return this.guuid;
    }
}

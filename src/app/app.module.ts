import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {InlineSVGModule} from 'ng-inline-svg';
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {NgbDropdownModule, NgbModalModule} from "@ng-bootstrap/ng-bootstrap";
import {CookieService} from 'ngx-cookie-service';

import {AppComponent} from './app.component';
import {WebsocketProviderService} from "./services/websocket-provider.service";
import {AppRoutingModule} from "./app-routing.module";
import {RegisterComponent} from "./register/register.component";
import {UserInfoService} from "./services/user-info.service";
import {GameFilterPipe} from "./game-filter.pipe";
import {GamesComponent} from "./games/games.component";
import {GameHeaderComponent} from "./game-header/game-header.component";

import {TeamComponent} from "./code-names/game/teams/team/team.component";
import {BoardComponent} from "./code-names/game/board/board.component";
import {TeamsComponent} from "./code-names/game/teams/teams.component";
import {GameService} from "./code-names/services/game.service";
import {GameGuard} from "./code-names/game/game.guard";
import {SpyComponent} from "./code-names/game/teams/team/spy/spy.component";
import {ActionsService} from "./code-names/services/actions.service";
import {CodeNamesGameDetailComponent} from "./code-names/game/game-detail/game-detail.component";
import {ValidationModalComponent} from './code-names/game/board/validation-modal/validation-modal.component';

import {UnoCardComponent} from "./uno/game/board/card/uno-card.component";
import {UnoGameService} from "./uno/services/uno-game.service";
import {UnoActionsService} from "./uno/services/uno-actions.service";
import {UnoGameGuard} from "./uno/game/uno-game.guard";
import {UnoGameDetailComponent} from "./uno/game/game-detail/game-detail.component";
import {UnoPlayersComponent} from "./uno/game/players/uno-players.component";
import {UnoBoardComponent} from "./uno/game/board/uno-board.component";
import {ColorSelectionModalComponent} from "./uno/game/board/card/color-selection-modal/color-selection-modal.component";
import {UnoColorButtonComponent} from "./uno/game/board/color-button/color-button.component";
import {UserGuard} from "./user.guard";


@NgModule({
    declarations: [
        AppComponent,
        RegisterComponent,
        GameFilterPipe,
        GamesComponent,
        GameHeaderComponent,

        CodeNamesGameDetailComponent,
        TeamsComponent,
        TeamComponent,
        BoardComponent,
        SpyComponent,
        ValidationModalComponent,

        UnoCardComponent,
        UnoGameDetailComponent,
        UnoPlayersComponent,
        UnoBoardComponent,
        ColorSelectionModalComponent,
        UnoColorButtonComponent
    ],
    imports: [
        AppRoutingModule,
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        InlineSVGModule.forRoot(),
        NgbModalModule,
        NgbDropdownModule
    ],
    providers: [
        WebsocketProviderService,
        UserInfoService,
        CookieService,
        UserGuard,

        GameService,
        ActionsService,
        GameGuard,

        UnoGameService,
        UnoActionsService,
        UnoGameGuard
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}

import {Component, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {UserInfoService} from "../services/user-info.service";

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {
    icons = [
        ['book', 'trophy', 'watch', 'wrench', 'star-fill'],
        ['music-note-beamed', 'lightning-fill', 'house-door-fill', 'heart-fill', 'hammer'],
        ['gift-fill', 'gem', 'film', 'cone-striped', 'cloud-fill'],
        ['camera-video-fill', 'briefcase-fill', 'alarm-fill', 'controller', 'bookmark-fill']
    ];
    formGroup: FormGroup;

    constructor(private userInfoService: UserInfoService,
                private fb: FormBuilder,
                private router: Router) {

    }

    ngOnInit(): void {
        const me = this.userInfoService.me;

        this.formGroup = this.fb.group({
            name: [me.name, Validators.required],
            icon: [me.icon, Validators.required]
        })
    }

    onSelectIcon(img: string) {
        this.formGroup.patchValue({icon: img}, {emitEvent: true});
    }

    onCreate() {
        if (this.formGroup.valid) {
            this.userInfoService.me = {...this.userInfoService.me, ...this.formGroup.value};
            this.router.navigate(['/games']);
        }
    }

}
